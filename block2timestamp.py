#!/usr/bin/python3

import argparse
import logging

from re import findall
from datetime import datetime
from sqlite3 import IntegrityError, connect as sqlconnect
from bs4 import BeautifulSoup as bs

from fetcher import get_block
from consts import AVG_BLOCK_TIME

class Block2Timestamp:
    def __init__(self):
        self.conn = sqlconnect('b2s.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('create table if not exists block2timestamp ("block" integer, "ts" integer, primary key("block"));')

    def add(self, block, timestamp):
        try:
            self.c.execute('insert into block2timestamp values (?, ?);', (block, timestamp))
            logging.info('block2timestamp: %i -> %i', block, timestamp)
            self.conn.commit()
        except IntegrityError:
            pass

    def get(self, block):
        try:
            self.c.execute('select ts from block2timestamp where block=?', (block,))
            timestamp = int(self.c.fetchall()[0][0])
            logging.info('found cached %i -> %i', block, timestamp)
            return timestamp
        except IndexError:
            logging.info('cache miss block %i', block)
            return None

    def fetchparse(self, block):
        raw = bs(get_block(block), features='lxml').find('i', {'class':'fa-clock'}).parent.contents[2]
        date = findall(r'.*\(([^)]+)\).*', raw)[0]
        timestamp = datetime.strptime(date, '%b-%d-%Y %I:%M:%S %p +%Z').timestamp()
        logging.info('Converted block %i to timestamp %i (%s)', block, timestamp, date)
        self.add(block, timestamp)
        return timestamp

    def guess(self, block):
        self.c.execute('select block, ts from block2timestamp order by block desc limit 1;')
        max_block, max_timestamp = map(int, self.c.fetchall()[0])
        if max_block >= block:
            raise RuntimeError('Trying to guess for non-future block')
        guessed = int(max_timestamp + (block - max_block)*AVG_BLOCK_TIME)
        logging.info('Guessing block %i at timestamp %i', block, guessed)
        return guessed

    def query(self, block):
        timestamp = self.get(block)
        if timestamp is None:
            try:
                timestamp = self.fetchparse(block)
            except AttributeError:
                timestamp = self.guess(block)
        return timestamp

def main():
    parser = argparse.ArgumentParser(description='Ethereum block to timestamp converter')
    parser.add_argument('block', help="the block to get the timestamp of", nargs=1)
    parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(levelname)s %(funcName)16s %(message)s')
    block = int(args.block[0])
    b2s = Block2Timestamp()
    timestamp = b2s.query(block)
    print(timestamp)

if __name__ == '__main__':
    main()
