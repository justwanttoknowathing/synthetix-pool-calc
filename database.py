from sqlite3 import connect as sqlconnect
from os import unlink
import logging

class Database:
    def __init__(self, safe=True):
        self._setup(safe)

    def _setup(self, safe=True):
        self.conn = sqlconnect('pooldb.sqlite', check_same_thread=safe)
        self.c = self.conn.cursor()
        self.c.execute('create table if not exists txs ("tx" string, "date" integer, "from_addr" string, "to_addr" string, "quantity" string, primary key("tx", "from_addr", "to_addr", "quantity"));')
        self.c.execute('create index if not exists txs_date on txs ("date");')

    def is_repeat(self, tx, from_addr, to_addr, quantity):
        self.c.execute('select tx from txs where tx=? and from_addr=? and to_addr=? and quantity=?', (tx, from_addr, to_addr, quantity))
        return len(self.c.fetchall()) >= 1

    def add(self, tx, date, from_addr, to_addr, quantity):
        if self.is_repeat(tx, from_addr, to_addr, quantity):
            logging.info('Repeat tx: %s', tx)
            return False
        logging.info('tx: %s, date: %s, from: %s, to: %s, quantity: %s', tx, date, from_addr, to_addr, quantity)
        self.c.execute('insert into txs values (?, ?, ?, ?, ?);', (tx, date, from_addr, to_addr, quantity))
        self.conn.commit()
        return True

    def query(self, q, p=None):
        if p is None:
            self.c.execute(q)
        else:
            self.c.execute(q, p)
        return self.c.fetchall()

    def unlink(self):
        unlink('pooldb.sqlite')
        self._setup()
