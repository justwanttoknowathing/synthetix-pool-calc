#!/usr/bin/python3

import argparse
import logging

from copy import deepcopy
from database import Database
from block2timestamp import Block2Timestamp
from consts import REWARD_AMOUNT, MIN_QUALIFYING_BALANCE, ZERO_ADDR

class Transaction:
    def __init__(self, txhash, time, from_addr, to_addr, quantity):
        self.hash = txhash
        self.time = int(time)
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.quantity = quantity
        self.sender = to_addr if self.is_add() else from_addr

    def is_add(self):
        return self.from_addr == ZERO_ADDR
 
    def is_remove(self):
        return self.to_addr == ZERO_ADDR

    def is_transfer(self):
        return not self.is_add() and not self.is_remove()

    def is_disqualifying(self):
        return self.is_remove() or self.is_transfer()

def init_balance(balances, addr):
    if addr not in balances:
        balances[addr] = 0

def update_balances(tx, balances):
    if tx.is_add():
        balances[tx.sender] += tx.quantity
        logging.info('%s +%.2f = %.2f', tx.sender, tx.quantity, balances[tx.sender])
    elif tx.is_remove():
        balances[tx.sender] -= tx.quantity
        logging.info('%s -%.2f = %.2f', tx.sender, tx.quantity, balances[tx.sender])
    elif tx.is_transfer():
        init_balance(balances, tx.to_addr)
        balances[tx.from_addr] -= tx.quantity
        balances[tx.to_addr] += tx.quantity
        logging.info('%s->%s %.02f = %.2f, %.2f', tx.from_addr, tx.to_addr, tx.quantity, balances[tx.from_addr], balances[tx.to_addr])

def do_disqualify(tx, in_period, disqualified):
    if in_period and tx.is_disqualifying():
        disqualified[tx.sender] = tx.hash
        logging.info('%s due to withdraw: %i, transfer: %i', tx.sender, tx.is_remove(), tx.is_transfer())

def only_qualified(balances, disqualified):
    return {k: v for k, v in balances.items() if k not in disqualified and v >= MIN_QUALIFYING_BALANCE}

def show_results(balances, disqualified, sort_snx):
    qualified_balances = only_qualified(balances, disqualified)
    total = sum(qualified_balances.values())
    if sort_snx:
        look_at = sorted(qualified_balances, key=lambda k: qualified_balances[k], reverse=True)
    else:
        look_at = sorted(qualified_balances)
    for k in look_at:
        bal = qualified_balances[k]
        print('%s: %.02f\t%.02f\t%.02f' % (k, bal, 100*bal/total, REWARD_AMOUNT*bal/total))

def handle_blocks_or_timestamps(start, end):
    start_len = len(start)
    end_len = len(end)
    start = int(start)
    end = int(end)
    b2s = Block2Timestamp()
    if start_len < 10:
        start = b2s.query(start)
    if end_len < 10:
        end = b2s.query(end)
    return start, end

def replay(start, end):
    start, end = handle_blocks_or_timestamps(start, end)
    db = Database(safe=False)
    txs = db.query('select * from txs order by date')
    balances = {}
    start_balances = None
    disqualified = {}
    for tx in [Transaction(txhash, time, from_addr, to_addr, quantity) for txhash, time, from_addr, to_addr, quantity in txs]:
        init_balance(balances, tx.sender)
        if tx.time >= start and start_balances is None:
            start_balances = deepcopy(balances)
            logging.info('Grabbing balances at %i', tx.time)
        update_balances(tx, balances)
        do_disqualify(tx, tx.time >= start and tx.time <= end, disqualified)
    if start_balances is None:
        start_balances = balances
    return start_balances, disqualified

def main():
    parser = argparse.ArgumentParser(description='Uniswap sETH/ETH pool calculator')
    parser.add_argument('blocks', help="blocks (or timestamps) between which to calculate sETH/ETH pool rewards", nargs=2)
    parser.add_argument("-q", "--quantity", help="sort by SNX reward instead of by address", action="store_true")
    parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(levelname)s %(funcName)16s %(message)s')
    start, end = args.blocks
    balances, disqualified = replay(start, end)
    show_results(balances, disqualified, args.quantity)

if __name__ == '__main__':
    main()
