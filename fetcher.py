from requests import get

# EC2 can't python-requests to etherscan, so this is a hack url using a port forward and /etc/hosts
ETHERSCAN = 'https://etherscan.io:44333'
HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}

def get_pool_page(n):
    pool_page_url = '/token/generic-tokentxns2?contractAddress=0xe9cf7887b93150d4f2da7dfc6d502b216438f244&mode=&m=normal&p='
    return get('%s%s%i' % (ETHERSCAN, pool_page_url, n), headers=HEADERS).text

def get_block(n):
    block_url = '/block/'
    return get('%s%s%i' % (ETHERSCAN, block_url, n), headers=HEADERS).text
