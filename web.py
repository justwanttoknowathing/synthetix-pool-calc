#!/usr/bin/python3

from flask import Flask, render_template, abort
from replay import replay, only_qualified
from consts import REWARD_AMOUNT

app = Flask(__name__)

@app.route('/logs')
def logs():
    with open('/home/ubuntu/synthetix-pool-calc/logs/watcher.log') as f:
        lines = [{'message': line.strip().expandtabs()} for line in f.readlines()]
        return render_template('logs.html', lines=lines[-50:])

@app.route('/pool-rewards/<start>/<end>')
def pool_rewards(start, end):
    try:
        balances, disqualified = replay(start, end)
    except:
        abort(403)

    unisum = sum(balances.values())
    qualified_balances = only_qualified(balances, disqualified)
    total = sum(qualified_balances.values())

    rewards = [{'address': i, \
                'balance':  '' if i not in qualified_balances else round(qualified_balances[i], 3), \
                'percent':  0  if i not in qualified_balances else round(100*qualified_balances[i]/total, 3), \
                'SNX':      0  if i not in qualified_balances else round(qualified_balances[i]/total*REWARD_AMOUNT, 2), \
                'disqualifiedbalance': '' if i not in disqualified and balances[i] >= 1 else round(balances[i], 3), \
                'disqualifyingtx': '' if i not in disqualified else disqualified[i]} \
              for i in balances if balances[i] > 0.000001]
    rewards = sorted(rewards, key=lambda d: d['SNX'] + (d['disqualifiedbalance']/100000 if not d['balance'] else 0), reverse=True)

    weeks = []
    i = 11
    with open('keyblocks') as f:
        for line in f:
            kstart, kend, note = line.split('\t')
            weeks.append({'week': i, 'start': kstart, 'end': kend, 'note': note})
            i += 1

    disqualified = [{'address': k, 'tx': v} for k, v in disqualified.items()]

    return render_template('pool-rewards.html', rewards=rewards, start=start, end=end, weeks=weeks, unisum=unisum)

if __name__ == '__main__':
    app.run(debug=False, threaded=True, host='0.0.0.0')
