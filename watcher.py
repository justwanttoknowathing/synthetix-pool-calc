#!/usr/bin/python3

import logging
import argparse

from datetime import datetime
from bs4 import BeautifulSoup as bs

from fetcher import get_pool_page
from database import Database

def parse_row(row):
    tx, date, _, from_addr, _, to_addr, quantity, _ = row.find_all('td')
    tx = tx.find('a').contents[0]
    date = datetime.strptime(date.find('span').contents[0], '%Y-%m-%d %H:%M:%S').timestamp()
    from_addr = from_addr.find('a').contents[0]
    to_addr = to_addr.find('a').contents[0]
    quantity = quantity.contents[0].replace(',', '')
    return tx, date, from_addr, to_addr, quantity

def gather(db, check_history):
    logging.info('Gathering history')
    n = 1
    while True:
        page = bs(get_pool_page(n), features='lxml').find('tbody')
        if 'There are no matching entries' in str(page):
            logging.info('Went beyond final page on #%i, done', n)
            break
        for row in page.find_all('tr'):
            tx, date, from_addr, to_addr, quantity = parse_row(row)
            added = db.add(tx, date, from_addr, to_addr, quantity)
            if not check_history and not added:
                logging.info('Saw repeat tx %s, stopping', tx)
                return
        n += 1

def main():
    parser = argparse.ArgumentParser(description='Uniswap sETH/ETH pool: daemon for backend updates')
    parser.add_argument("-f", "--full-history", help="check all transactions instead of stopping upon encountering a repeat transaction", action="store_true")
    parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(funcName)12s\t%(message)s')
    db = Database()
    if args.full_history:
        db.unlink()
    gather(db, args.full_history)

if __name__ == '__main__':
    main()
